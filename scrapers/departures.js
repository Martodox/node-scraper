var sprintf = require("sprintf-js").vsprintf;

function zeroPad(num, places) {
    if (!places) places = 4;
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
};

module.exports = {

    getHours: function (request, cheerio, line, callback) {

        line = zeroPad(line);
        urlTemplate = 'http://rozklady.mpk.krakow.pl/aktualne/%s/%st';

        url = sprintf(urlTemplate, [line, line]);
        dataSet = [];
        current = 1;
        timeStart = new Date();
        var $hours = [];
        makeRequest = function (url) {
            currentStop = zeroPad(current++, 3);
            reqUrl = url + currentStop + '.htm';
            request(reqUrl, function (error, response, html) {
                if (!error && response.statusCode == 200) {
                    $hours.push(html);
                    makeRequest(url);
                } else {
                    timeStart = new Date() - timeStart;
                    saveData($hours);
                }
            });
        };

        makeRequest(url, callback);

        saveData = callback;


    },

    getLines: function (request, cheerio, callback) {
        var url = 'http://rozklady.mpk.krakow.pl/linie.aspx';
        request(url, function (error, response, html) {
            if (!error) {
                var $ = cheerio.load(html);
                var stops = $('a');
                var lineList = [];
                stops.each(function () {
                    if ($(this).text().length < 4) {
                        lineList.push({
                            name: $(this).text()
                        });
                    }
                });
                callback(lineList, 0, new Date());
            }
        });
    },


    parseHours: function (payload, cheerio) {
        var $ = cheerio.load(payload);
        var $rows = [];
        var $days = 0;

        //check if sundays are on the list too
        $('.celldepart tr:first-child td').each(function (e) {
            if ($(this).text().trim().length > 1) {
                $days++;
            }
        });

        //extract each row of time departures into an array
        $('.celldepart tr:not(:first-child) td').each(function (e) {
            if ($(this).text().length < 90) {
                $rows.push($(this).text().trim());
            }
        });

        var $tmp = [];
        for (var $i = 0; $i < $days; $i++) {
            $tmp[$i] = [];
        }

        var $hours = [];

        var $i = 0,
            $z = 0,
            $k = 0;

        $rows.forEach(function ($row) {
            $i++;

            if ($i % 2 != 0) {
                $tmp[$k][$z] = $row;
            } else {
                $tmp[$k][$z] = $row.split(' ');
                $k++;
            }

            if ($z++ >= 1) {
                $z = 0;
            }

            if ($k > $days - 1) {
                $k = 0;
            }

            if ($i > $days * 2 - 1) {
                $i = 0;
                $tmp.forEach(function ($hour, i) {
                    $hour[1].forEach(function ($minute) {
                        if ($minute != '-') {
                            $hours.push({
                                hour: $hour[0],
                                minute: $minute,
                                dayflag: i
                            });
                        }
                    });
                });
            }
        });

        return $hours;


    }


};

