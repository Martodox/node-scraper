module.exports = {

    getStops: function (request, cheerio, callback) {
        var url = 'http://rozklady.mpk.krakow.pl/aktualne/przystan.htm';

        request(url, function (error, response, html) {
            if (!error) {
                var $ = cheerio.load(html);
                var stops = $('table li a');
                callback(stops);
            }
        });

    }

};

