var Sequelize = require('sequelize');

var sequelize = new Sequelize('node', 'root', 'root', {
    dialect: "mysql",
    port: 3306
});

return sequelize.define('Departure', {
        stop_id: Sequelize.INTEGER,
        line_id: Sequelize.INTEGER,
        route_id: Sequelize.INTEGER,
        hour: Sequelize.STRING,
        minute: Sequelize.STRING,
        hhmm: Sequelize.STRING,
        dayflag: Sequelize.INTEGER
    },
    {
        timestamps: false
    });

