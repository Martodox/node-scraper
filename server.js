var request = require('request'),
    cheerio = require('cheerio'),
    Sequelize = require('sequelize'),
    $jproxy = require('nodeproxy'),
    departuresScrap = require('./scrapers/departures');

var log = function (data) {
    console.log(data);
};


var sequelize = new Sequelize('node', 'root', 'root', {
    dialect: "mysql",
    port: 3306,
    logging: false
});


var Departure = sequelize.define('Departure', {
        stop_id: Sequelize.INTEGER,
        line_id: Sequelize.INTEGER,
        route_id: Sequelize.INTEGER,
        hour: Sequelize.STRING,
        minute: Sequelize.STRING,
        dayflag: Sequelize.INTEGER
    },
    {
        timestamps: false
    });


request('http://localhost/test.htm', function (error, response, html) {
    //var $something = departuresScrap.parseHours(html, cheerio);
    // Departure.bulkCreate($something);
});


totalTime = 0;
function linesList(data, current, date) {
    departuresScrap.getHours(request, cheerio, data[current].name, function (stopsList, timeElapsed) {


        stopsList.forEach(function (stopsList) {
            Departure.bulkCreate(departuresScrap.parseHours(stopsList, cheerio));
        });

        $timeElapsed = (new Date - date) / 1000;

        log(data[current].name + ' ' + $timeElapsed + 's');
        current++;

        if (current < data.length) {
            linesList(data, current, date);
        }

    });
};


departuresScrap.getLines(request, cheerio, linesList);


//stopsScrap.getStops(request, cheerio, function (data) {
//    log(data.length);
//});


